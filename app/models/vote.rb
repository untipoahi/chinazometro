class Vote < ActiveRecord::Base
  belongs_to :users
  belongs_to :chinazos
  attr_accessible :value, :users_id, :chinazos_id

  def saveOrUpdate
  	conditionString = 'users_id = ? and chinazos_id = ?', self.users_id, self.chinazos_id
  	@votes = Vote.find(:first, :conditions => conditionString )
  	if @votes
      if 5.minutes.ago - @votes.created_at < 0
    	  @votes.value = self.value
    	  return @votes.save
      else
        return false
      end
    else
    	return self.save
  	end
  end  
end
