class Chinazo < ActiveRecord::Base
	has_many :comentario
	attr_accessor :acusador, :puntos, :cantidad_comentarios
	attr_accessible :descripcion, :nombre
	validates :nombre, presence: true, length: { minimum: 3 }
	#before_save :serUserId

	def puntos
		@puntos = Vote.sum(:value,  :conditions =>"chinazos_id = #{id}")
		if @puntos.nil?
			self.puntos = 0
		else
			self.puntos = @puntos	
		end
	end

	def acusador
		User.find(users_id).nombre if users_id
	end

	def cantidad_comentarios
		Comentario.where("chinazos_id = #{id}").count if self.id
	end

	def puntos_by_user(user_id)
	    #ActiveRecord::Base.logger.debug "Comprobando votos usuario: #{user_id}" 
	    @vote = Vote.where("chinazos_id = ? and users_id= ?", id, user_id).first
		if @vote
			@vote.value if self.id
		else
			return 0
		end
	end
end
